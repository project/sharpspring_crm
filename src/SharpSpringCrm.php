<?php

namespace Drupal\sharpspring_crm;

use Drupal\webform\Entity\WebformSubmission;
use GuzzleHttp\Exception\GuzzleException;

class SharpSpringCrm {

  /**
   * Configuration object containing the variables
   */
  protected $config;

  /**
   * SharpSpringCrm constructor.
   */
  public function __construct()
  {
    $configFactory = \Drupal::configFactory();
    $this->config = $configFactory->getEditable('sharpspring_crm.settings');
  }

  /**
   * Get List of Existing Email Lists
   */
  public function getLists(): array
  {

    try {
      // Implements SharpSpring getActiveLists
      $client = \Drupal::httpClient();
      $method = 'getActiveLists';
      $queryString = http_build_query(array('accountID' => $this->config->get("account_id"), 'secretKey' => $this->config->get("secret_key")));
      $url = 'http://api.sharpspring.com/pubapi/v1/?' . $queryString;

      $data = new \stdClass();
      $data->method = $method;
      $data->id = rand();
      $data->params = ['where' => []];
      $request = $client->post($url, [
        'json' => $data
      ]);
      $lists = json_decode($request->getBody(), true);

      // Transform results into array of IDs and Names
      $activeLists = [];
      foreach ($lists['result']['activeList'] as $key => $list) {
        $activeLists[$list['id']] = $list['name'];
      }

      return $activeLists;

    } catch (GuzzleException $exception) {
      throw $exception;
    }
  }

  /**
   * Implements SharpSpring getFields
   */
  public function getFields(): array
  {
    try {
      // Get Active Lists from SharpSpring
      $client = \Drupal::httpClient();
      $method = 'getFields';
      $queryString = http_build_query(array('accountID' => $this->config->get("account_id"), 'secretKey' => $this->config->get("secret_key")));
      $url = 'http://api.sharpspring.com/pubapi/v1/?' . $queryString;

      $data = new \stdClass();
      $data->method = $method;
      $data->id = rand();
      $data->params = new \stdClass();
      $data->params->where = new \stdClass();
      $request = $client->post($url, [
        'json' => $data
      ]);

      return json_decode($request->getBody(), true);

    } catch (GuzzleException $exception) {
      throw $exception;
    }
  }

  /**
   * Implements SharpSpring addListMemberEmailAddress
   */
  public function addListMemberEmailAddress(string $listId, string $emailAddress) {
    $client = \Drupal::httpClient();
    $method = 'addListMemberEmailAddress';
    $queryString = http_build_query(array('accountID' => $this->config->get("account_id"), 'secretKey' => $this->config->get("secret_key")));
    $url = 'http://api.sharpspring.com/pubapi/v1/?' . $queryString;

    $data = new \stdClass();
    $data->method = $method;
    $data->id = rand();
    $data->params = ['listID' => $listId, 'emailAddress' => $emailAddress];
    $request = $client->post($url, [
      'json' => $data
    ]);
    $result = $request->getStatusCode();
  }

  /**
   * Implements SharpSpring createLeads
   * @todo Add Update if exists?
   */
  public function createLeads(\stdClass $leadToSend, WebformSubmission $webform_submission)
  {
    try {
      $client = \Drupal::httpClient();
      $method = 'createLeads';
      $queryString = http_build_query(array('accountID' => $this->config->get("account_id"), 'secretKey' => $this->config->get("secret_key")));
      $url = 'http://api.sharpspring.com/pubapi/v1/?' . $queryString;

      $data = new \stdClass();
      $data->method = $method;
      $data->id = rand();
      $data->params = new \stdClass();
      $data->params->objects = [$leadToSend];
      $request = $client->post($url, [
        'json' => $data
      ]);
      $response = json_decode($request->getBody());

      // Track Results
      switch ($response->result->creates[0]->success) {
        case TRUE:
          return true;
        case FALSE:
          $error = $response->error[0]->code . ': ' . $response->error[0]->message;
          \Drupal::logger('sharpspring_crm')->notice($error);
          return false;
      }
    } catch (GuzzleException $e) {
      \Drupal::logger('sharpspring_crm')->error($e->getMessage());
      throw $e;
    }
  }
}
