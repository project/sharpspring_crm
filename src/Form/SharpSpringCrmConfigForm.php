<?php

namespace Drupal\sharpspring_crm\Form;

use Drupal\Core\Form\{ConfigFormBase, FormStateInterface};
use Drupal\sharpspring_crm\SharpSpringCrm;


class SharpSpringCrmConfigForm extends ConfigFormBase {

  /**
   * @return string
   */
  public function getFormId(): string {
    return "sharpspring_crm";
  }

  /**
   * @return array
   */
  public function getEditableConfigNames(): array {
    return [
      "sharpspring_crm.settings"
    ];
  }

  /**
   * @param array $form
   * @param FormStateInterface $formState
   * @return array
   */
  public function buildForm(array $form, ?FormStateInterface $formState): array {
    $config = \Drupal::config("sharpspring_crm.settings");

    $form["sharpspring_crm_settings"]["account_id"] = [
      "#type" => "textfield",
      "#title" => $this->t("Account Id"),
      "#default_value" => $config->get("account_id"),
      "#required" => TRUE
    ];

    $form["sharpspring_crm_settingsorm_settings"]["secret_key"] = [
      "#type" => "textfield",
      "#title" => $this->t("Secret Key"),
      "#default_value" => $config->get("secret_key"),
      "#required" => TRUE
    ];

    $form["sharpspring_crm_settingsorm_settings"]["backupEmailAddress"] = [
      "#type" => "textfield",
      "#title" => $this->t("Backup Email Address"),
      '#description' => $this->t('Enter a list (separated by semicolons) to send backup emails to if connection fails.'),
      "#default_value" => $config->get("backupEmailAddress"),
      "#required" => TRUE
    ];

    return parent::buildForm($form, $formState);
  }

  public function submitForm(array &$form, ?FormStateInterface $formState) {
    $values = $formState->getValues();

    $this->configFactory->getEditable("sharpspring_crm.settings")
      ->set("account_id", $values["account_id"])
      ->set("secret_key", $values["secret_key"])
      ->set("backupEmailAddress", $values["backupEmailAddress"])
      ->save();

    parent::submitForm($form, $formState);
  }
}
