<?php

namespace Drupal\sharpspring_crm\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\sharpspring_crm\SharpSpringCrm;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form submission handler.
 *
 * @WebformHandler(
 *   id = "sharpspring_crm_list_handler",
 *   label = @Translation("SharpSpring List Handler"),
 *   category = @Translation("Marketing"),
 *   description = @Translation("Adds User Email to SharpSpring Email List."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 * )
 */

class SharpSpringCrmListHandler extends WebformHandlerBase {

  /**
   * The token manager.
   *
   * @var \Drupal\webform\WebformTokenManagerInterface
   */
  protected $tokenManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->tokenManager = $container->get('webform.token_manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [];
  }

  /**
   * {@inheritdoc}
   * @throws GuzzleException
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state)
  {
    $form = parent::buildConfigurationForm($form, $form_state);

    $sswf = new SharpSpringCrm();
    $form['sharpspring'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('SharpSpring settings'),
    ];

    $form['sharpspring']['description'] = [
      '#markup' => '<p>This handler expects a field with an email address with a key of <strong>email_address</strong>'
    ];

    $form['sharpspring']['active_list'] = [
      '#type' => 'select',
      "#title" => $this->t("List to subscribe"),
      '#options' => $sswf->getLists(),
      '#default_value' => $this->configuration['active_list']
    ];

    return $this->setSettingsParents($form);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void
  {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['active_list'] = $form_state->getValue(['sharpspring', 'active_list']);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission)
  {
    // Get config for module
    $listId = $this->configuration['active_list'];

    // Get Submitted Email Address
    $emailAddress = $form_state->getValue('email_address');

    if (!empty(($emailAddress))) {
      $sswf = new SharpSpringCrm();
      $sswf->addListMemberEmailAddress($listId, $emailAddress);
    }

  }

}
