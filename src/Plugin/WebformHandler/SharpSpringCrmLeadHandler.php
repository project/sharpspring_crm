<?php

namespace Drupal\sharpspring_crm\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\sharpspring_crm\SharpSpringCrm;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form submission handler.
 *
 * @WebformHandler(
 *   id = "sharpspring_crm_lead_handler",
 *   label = @Translation("SharpSpring Lead Handler"),
 *   category = @Translation("Marketing"),
 *   description = @Translation("Adds/Updated Lead in SharpSpring."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 * )
 */

class SharpSpringCrmLeadHandler extends WebformHandlerBase
{

  /**
   * The token manager.
   *
   * @var \Drupal\webform\WebformTokenManagerInterface
   */
  protected $tokenManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
  {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->tokenManager = $container->get('webform.token_manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration()
  {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state)
  {
    $form = parent::buildConfigurationForm($form, $form_state);

    // Get Module Configuration
    $config = \Drupal::config("sharpspring_crm.settings");

    // Get list of fields from SharpSpring Lead
    try {
      $sharpspring = new SharpSpringCrm();
      $fields = $sharpspring->getFields();
    } catch (GuzzleException $e) {
      $markup = '<div ><h3>We didnt found any relevant fields, please check your account Id and secret key.</h3></div>';
      $form['error_markup'] = [
        '#type' => 'markup',
        '#markup' => $markup,
        '#weight' => 1,
      ];
    }

    $form['sharpspring'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('SharpSpring CRM configuration'),
    ];
    $form['sharpspring']['instructions'] = [
      '#markup' => '<p>Enter the machine name of the webform field to each' .
        'SharpSpring field you wish to map.',
    ];

    if (count($fields['result']['field']) > 0) {
      foreach ($fields['result']['field'] as $key => $field) {
        if ((bool)$field['isActive']) {
          $form['sharpspring'][$field['systemName']] = [
            '#type' => 'textfield',
            '#title' => $this->t($field['label']),
            '#required' => (bool)$field['isRequired'],
            '#default_value' => $this->configuration[$field['systemName']]
          ];
        }
      }
    } else {
      $form['error_markup'] = [
        '#markup' => '<p>There are no fields to configure for this API ID</p>'
      ];
    }
    return $this->setSettingsParents($form);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void
  {
    parent::submitConfigurationForm($form, $form_state);


    // Clear array to re-save new values
    $this->configuration = array();

    // Save Configurations for Handler
    $fields = $form_state->getValues()['sharpspring'];
    foreach($fields as $key => $field) {
      if(!empty($field)) {
        $this->configuration[$key] = $field;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE): void
  {
    // Get Module Configuration
    $config = \Drupal::config("sharpspring_crm.settings");

    // Retreive data to send to SharpSpring
    if($webform_submission->isCompleted()) {
      $dataToSend = new \stdClass();
      $formData = $webform_submission->getData();

      // Loop through values and fields
      foreach ($this->configuration as $ssKey => $ssValue) {
        $dataToSend->{$ssKey} = $formData[$ssValue];
      }

      $sharpspringCrm = new SharpSpringCrm();
      if(!$sharpspringCrm->createLeads($dataToSend, $webform_submission)) {
        // Send email to admin if lead did not successfully send to SharpSpring
        $mailManager = \Drupal::service('plugin.manager.mail');
        $linkToSumbission = $webform_submission->toLink($this->t('View Submission'), 'canonical',
          ['absolute' => TRUE, 'attributes' => ['target' => '_blank']])->toString();
        $module = 'sharpspring_crm';
        $key = 'backup_email';
        $to = $config->get('backupEmailAddress');
        $params['message'] = 'A new lead was submitted on your website but didnt make it to SharpSpring. '
          . 'The submission can be found at: ' . $linkToSumbission->getGeneratedLink();
        $params['subject'] = 'New Lead from Website';
        $langcode = \Drupal::currentUser()->getPreferredLangcode();
        $mailManager->mail($module, $key, $to, $langcode, $params, NULL, true);
      }
    }
  }
}
